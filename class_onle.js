class Person {

    constructor (person){
        this.firstName = person[0];
        this.lastName = person[1];
    }

    show() {
        for (let key in this) {
            console.log(this[key]); 
        }
    }

    peoplesQuantity() {
        console.log('Всего людей: ' + peoples.length)
    }
   
}

class Student extends Person {
    
    constructor (person, dateOfBirth, course) {
        super(person);
        this.dateOfBirth = dateOfBirth;
        this.course = course;
    }

}

class Worker extends Person {
    
    constructor (person, salary, payouts) {
        super(person);
        this._salary = salary;
        this.payouts = payouts;
    }
   
    get salary() {
        return this._salary;
      }
    
    setNewPayout(newPayouts, summ) {
        this.payouts.push([newPayouts, summ])
    }


}

class Manager extends Worker {

    constructor (person, salary, payouts, subordinates) {
        super(person, salary, payouts);
        this.subordinates = subordinates
    }

    setDelSubordinate(firstName, lastName) {
        this.subordinates.forEach((subordinate, index, subordinates) => {
           if (subordinate[0] === firstName && subordinate[1] === lastName) {
            subordinates.splice(index, 1)
           }
       });
    }

    setAddSubordinate(firstName, lastName) {
        this.subordinates.push([firstName, lastName])
    }

}

let peoples = [
    new Person (["Иван", "Петров"]),

    new Student (["Антон", "Антонов"], new Date(2002, 2, 25), 4),

    new Worker (["Валерий", "Валерьянов"], 80000, [[new Date(2021, 0, 10), 100000], [new Date(2021, 1, 10), 80000], [new Date(2021, 2, 10), 70000], [new Date(2021, 3, 10), 80000]]),

    new Manager (["Аркадий", "Смирнов"], 120000, [[new Date(2021, 0, 10), 140000], [new Date(2021, 1, 10), 120000], [new Date(2021, 2, 10), 110000], [new Date(2021, 3, 10), 130000]], [["Андрей", "Ильин"], ["Валерий", "Валерьянов"]])
]




// для примера новой выплаты 
peoples[3].setNewPayout(new Date(2021, 3, 10), 150000);

// удаление подчиненного
peoples[3].setDelSubordinate("Валерий", "Валерьянов");

// добавление подчиненного
peoples[3].setAddSubordinate("Олег", "Чиркашкин")


for (let i = 0; i < peoples.length; i++) {
    peoples[i].show()
}

peoples[3].peoplesQuantity()