import {Person} from './person.js';

export class Student extends Person {
    
    constructor (person, dateOfBirth, course) {
        super(person);
        this.dateOfBirth = dateOfBirth;
        this.course = course;
    }

}

