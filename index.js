import {Person} from './person.js';
import {Student} from './student.js';
import {Worker} from './worker.js';
import {Manager} from './manager.js';

let peoples = [
    new Person (["Иван", "Петров"]),

    new Student (["Антон", "Антонов"], new Date(2002, 2, 25), 4),

    new Worker (["Валерий", "Валерьянов"], 80000, [[new Date(2021, 0, 10), 100000], [new Date(2021, 1, 10), 80000], [new Date(2021, 2, 10), 70000], [new Date(2021, 3, 10), 80000]]),

    new Manager (["Аркадий", "Смирнов"], 120000, [[new Date(2021, 0, 10), 140000], [new Date(2021, 1, 10), 120000], [new Date(2021, 2, 10), 110000], [new Date(2021, 3, 10), 130000]], [["Андрей", "Ильин"], ["Валерий", "Валерьянов"]])
];

// примеры:

// для примера новой выплаты 
// peoples[3].setNewPayout(new Date(2021, 3, 10), 150000);

// удаление подчиненного
// peoples[3].setDelSubordinate("Валерий", "Валерьянов");

// добавление подчиненного
// peoples[3].setAddSubordinate("Олег", "Чиркашкин");


// выдод данных по всем людям
// for (let i = 0; i < peoples.length; i++) {
//     peoples[i].show()
// }

// посчет всех людей
// peoples[0].peoplesQuantity()

