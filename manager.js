import {Worker} from './worker.js';

export class Manager extends Worker {

    constructor (person, salary, payouts, subordinates) {
        super(person, salary, payouts);
        this.subordinates = subordinates
    }

    setDelSubordinate(firstName, lastName) {
        this.subordinates.forEach((subordinate, index, subordinates) => {
           if (subordinate[0] === firstName && subordinate[1] === lastName) {
            subordinates.splice(index, 1)
           }
       });
    }

    setAddSubordinate(firstName, lastName) {
        this.subordinates.push([firstName, lastName])
    }

}