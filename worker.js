import {Person} from './person.js';

export class Worker extends Person {
    
    constructor (person, salary, payouts) {
        super(person);
        this._salary = salary;
        this.payouts = payouts;
    }
   
    get salary() {
        return this._salary;
      }
    
    setNewPayout(newPayouts, summ) {
        this.payouts.push([newPayouts, summ])
    }


}